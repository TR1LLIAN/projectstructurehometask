﻿using Newtonsoft.Json;
using Projects.DAL.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace Projects.DAL.Context
{
    public class ProjectWebApiContext
    {
        private static ProjectWebApiContext _projectWebApiContext;

        public ProjectWebApiContext()
        {
            Teams = new List<Team>();
            Projects = new List<Project>();
            Users = new List<User>();
            Tasks = new List<Task>();

            Teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText(@"..\Projects.DAL\JsonFiles\teams.json"));
            Tasks = JsonConvert.DeserializeObject<List<Models.Task>>(File.ReadAllText(@"..\Projects.DAL\JsonFiles\tasks.json"));
            Users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(@"..\Projects.DAL\JsonFiles\users.json"));
            Users.ForEach(x => x.Name = x.FirstName);
            Projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText(@"..\Projects.DAL\JsonFiles\projects.json"));
        }

        public static ProjectWebApiContext GetProjectWebApiContext()
        {
            if (_projectWebApiContext != null)
            {
                return _projectWebApiContext;
            }
            _projectWebApiContext = new ProjectWebApiContext();

            return _projectWebApiContext;
        }

        public List<Team> Teams { get; set; }

        public List<Project> Projects { get; set; }

        public List<User> Users { get; set; }

        public List<Models.Task> Tasks { get; set; }

    }
}
