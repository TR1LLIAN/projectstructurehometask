﻿using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects.DAL
{
    public class Repository<T> : IRepository<T> where T : BaseModel
    {
        private readonly List<T> _items;

        public Repository(List<T> items)
        {
            _items = items;
        }

        public void Create(T item)
        {
            if (_items.Any(it => it.Id == item.Id))
            {
                throw new InvalidOperationException("Such entry already exist!");
            }
            _items.Add(item);
        }

        public void Delete(int id)
        {
            T deletedItem = _items.FirstOrDefault(it => it.Id == id);
            if(deletedItem is null)
            {
                throw new InvalidOperationException("Cannot delete not existing item!");
            }
            _items.Remove(deletedItem);
        }

        public T GetItemById(int id)
        {
            return _items.FirstOrDefault(it => it.Id == id);
        }

        public List<T> GetList()
        {
            return _items;
        }

        public void Update(T item)
        {
            var updatedItem = _items.FirstOrDefault(it => it.Id == item.Id);
            if( updatedItem != null)
            {
                _items[_items.IndexOf(updatedItem)] = item;
            }
        }
    }
}
