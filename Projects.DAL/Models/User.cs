﻿using System;

namespace Projects.DAL.Models
{

    public class User:BaseModel
    {
        public User()
        {
            Name = FirstName;
        }

        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }

}
