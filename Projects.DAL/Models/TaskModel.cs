﻿using System;

namespace Projects.DAL.Models
{
    public class TaskModel:BaseModel
    {
        public int PerformerId { get; set; }
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public User Performer { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
