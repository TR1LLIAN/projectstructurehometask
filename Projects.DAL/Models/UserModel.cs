﻿using System.Collections.Generic;

namespace Projects.DAL.Models
{
    public class UserModel
    {
        public User User { get; set; }
        public List<TaskModel>? UnFinishedTasks { get; set; }
        public Project? LastProject { get; set; }
        public TaskModel? LongestTask { get; set; }
    }
}
