﻿using System;
using System.Collections.Generic;

namespace Projects.DAL.Models
{
    public class ProjectsAggregatedData
    {
        public Project Project { get; }
        public User Author { get; }
        public Task Tasks { get; }
        public Team Team { get; }
        public User Performer { get; }

        public ProjectsAggregatedData(Project project, User author, Task tasks, Team team, User performer)
        {
            Project = project;
            Author = author;
            Tasks = tasks;
            Team = team;
            Performer = performer;
        }

        public override bool Equals(object obj)
        {
            return obj is ProjectsAggregatedData other &&
                   EqualityComparer<Project>.Default.Equals(Project, other.Project) &&
                   EqualityComparer<User>.Default.Equals(Author, other.Author) &&
                   EqualityComparer<Task>.Default.Equals(Tasks, other.Tasks) &&
                   EqualityComparer<Team>.Default.Equals(Team, other.Team) &&
                   EqualityComparer<User>.Default.Equals(Performer, other.Performer);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Project, Author, Tasks, Team, Performer);
        }
    }
}
