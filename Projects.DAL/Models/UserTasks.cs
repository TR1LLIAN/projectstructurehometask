﻿using System.Collections.Generic;

namespace Projects.DAL.Models
{
    public class UserTasks
    {
        public User User { get; set; }
        public List<TaskModel> Tasks { get; set; }
    }
}
