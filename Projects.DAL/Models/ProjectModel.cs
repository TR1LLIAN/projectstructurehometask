﻿using System.Collections.Generic;

namespace Projects.DAL.Models
{
    public class ProjectModel
    {
        public Project Project { get; set; }
        public User Author { get; set; }
        public List<Models.TaskModel> Tasks { get; set; }
        public TeamModel Team { get; set; }

    }
}
