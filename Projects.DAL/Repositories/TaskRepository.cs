﻿using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.DAL.Repositories
{
    public class TaskRepository:Repository<Task>,ITaskRepository
    {
        private List<Task> _tasks;
        public TaskRepository(List<Task> tasks) : base(tasks)
        {
            _tasks = tasks;
        }
    }
}
