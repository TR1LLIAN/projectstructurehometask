﻿using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private List<User> _users;
        public UserRepository(List<User> users):base(users)
        {
            _users = users;
        }
    }
}
