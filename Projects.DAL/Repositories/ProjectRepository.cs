﻿using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.DAL.Repositories
{
    public class ProjectRepository:Repository<Project>, IProjectRepository
    {
        private List<Project> _projects;
        public ProjectRepository(List<Project> projects) : base(projects)
        {
            _projects = projects;
        }
    }
}
