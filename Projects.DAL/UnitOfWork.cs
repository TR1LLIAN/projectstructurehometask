﻿
using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using Projects.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ProjectWebApiContext _projectWebApiContext;
        public Guid InstanceID { get; private set; }

        public UnitOfWork()
        {
            _projectWebApiContext = new ProjectWebApiContext();

            Users = new UserRepository(_projectWebApiContext.Users);
            Projects = new ProjectRepository(_projectWebApiContext.Projects);
            Tasks = new TaskRepository(_projectWebApiContext.Tasks);
            Teams = new TeamRepository(_projectWebApiContext.Teams);

            this.InstanceID = Guid.NewGuid();
        }

        public IUserRepository Users { get; }

        public ITaskRepository Tasks { get; }

        public ITeamRepository Teams { get; }

        public IProjectRepository Projects { get; }
    }
}
