﻿using Projects.DAL.Models;

namespace Projects.DAL.Interfaces
{
    public interface ITeamRepository:IRepository<Team>
    {
    }
}
