﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get; }
        ITaskRepository Tasks { get; }
        ITeamRepository Teams { get; }
        IProjectRepository Projects { get; }
    }
}
