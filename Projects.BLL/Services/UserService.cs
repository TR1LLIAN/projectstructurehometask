﻿using Newtonsoft.Json;
using Projects.BLL.Interfaces;
using Projects.DAL;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Services
{
    public class UserService:IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Create(User user)
        {
            _unitOfWork.Users.Create(user);
        }

        public void Delete(int id)
        {
            _unitOfWork.Users.Delete(id);
        }

        public List<User> GetUsers()
        {
            return _unitOfWork.Users.GetList();
        }

        public void Update(User user)
        {
            _unitOfWork.Users.Update(user);
        }
    }
}
