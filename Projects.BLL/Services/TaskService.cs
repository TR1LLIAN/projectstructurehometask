﻿using Projects.BLL.Interfaces;
using Projects.DAL;
using Projects.DAL.Interfaces;
using System.Collections.Generic;

namespace Projects.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TaskService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Create(DAL.Models.Task task)
        {
            _unitOfWork.Tasks.Create(task);
        }

        public void Delete(int id)
        {
            _unitOfWork.Tasks.Delete(id);
        }

        public List<DAL.Models.Task> GetTasks()
        {
            return _unitOfWork.Tasks.GetList();
        }

        public void Update(DAL.Models.Task task)
        {
            _unitOfWork.Tasks.Update(task);
        }
    }
}
