﻿using Projects.BLL.Interfaces;
using Projects.DAL;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProjectService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Create(Project project)
        {
            _unitOfWork.Projects.Create(project);
        }

        public void Delete(int id)
        {
            _unitOfWork.Projects.Delete(id);
        }

        public List<Project> GetProjects()
        {
            return _unitOfWork.Projects.GetList();
        }

        public void Update(Project project)
        {
            _unitOfWork.Projects.Update(project);
        }
    }
}
