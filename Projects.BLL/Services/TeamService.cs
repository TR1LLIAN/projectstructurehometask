﻿using Projects.BLL.Interfaces;
using Projects.DAL;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TeamService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Create(Team team)
        {
            _unitOfWork.Teams.Create(team);
        }

        public void Delete(int id)
        {
            _unitOfWork.Teams.Delete(id);
        }

        public List<Team> GetTeams()
        {
            return _unitOfWork.Teams.GetList();
        }

        public void Update(Team team)
        {
            _unitOfWork.Teams.Update(team);
        }
    }
}
