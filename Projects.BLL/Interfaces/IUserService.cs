﻿using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Interfaces
{
    public interface IUserService
    {
        List<User> GetUsers();
        void Delete(int id);
        void Create(User user);
        void Update(User user);
    }
}
