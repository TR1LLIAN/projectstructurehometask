﻿using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Interfaces
{
    public interface ILinqService
    {
        Dictionary<Project, int> Task1(int id);
        List<TaskModel> Task2(int id);
        List<TaskModel> Task3(int id);
        List<TeamModel> Task4();
        List<UserTasks> Task5();
        UserModel Task6(int id);
        List<ProjectInfo> Task7();
    }
}
