﻿using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Interfaces
{
    public interface ITeamService
    {
        List<Team> GetTeams();
        void Delete(int id);
        void Create(Team team);
        void Update(Team team);
    }
}
