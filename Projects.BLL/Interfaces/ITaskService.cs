﻿using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Interfaces
{
    public interface ITaskService
    {
        List<Task> GetTasks();
        void Delete(int id);
        void Create(Task task);
        void Update(Task task);
    }
}
