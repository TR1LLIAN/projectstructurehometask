﻿using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects.BLL.Interfaces
{
    public interface IProjectService
    {
        List<Project> GetProjects();
        void Delete(int id);
        void Create(Project project);
        void Update(Project project);
    }
}
