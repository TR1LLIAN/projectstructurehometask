﻿using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using Projects.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProjectsWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly ILinqService _linqService;

        public LinqController(ILinqService linqService)
        {
            _linqService = linqService;
        }

        [Route("Task1")]
        [HttpGet]
        public ActionResult<Dictionary<Project, int>> Get(int id)
        {
            return Ok(_linqService.Task1(id).ToList());
        }

        [Route("Task2")]
        [HttpGet]
        public ActionResult<List<TaskModel>> GetTasks(int id)
        {
            return Ok(_linqService.Task2(id));
        }

        [Route("Task3")]
        [HttpGet]
        public ActionResult<List<TaskModel>> GetFinishedTasks(int id)
        {
            return Ok(_linqService.Task3(id));
        }

        [Route("Task4")]
        [HttpGet]
        public ActionResult<List<TeamModel>> GetTeams()
        {
            return Ok(_linqService.Task4());
        }

        [Route("Task5")]
        [HttpGet]
        public ActionResult<List<TeamModel>> GetUsers()
        {
            return Ok(_linqService.Task5());
        }

        [Route("Task6")]
        [HttpGet]
        public ActionResult<List<TeamModel>> GetUser(int id)
        {
            return Ok(_linqService.Task6(id));
        }

        [Route("Task7")]
        [HttpGet]
        public ActionResult<List<TeamModel>> GetProjects()
        {
            return Ok(_linqService.Task7());
        }


    }
}
