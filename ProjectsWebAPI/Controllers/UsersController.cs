﻿using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;

namespace ProjectsWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        public ActionResult<List<User>> Get()
        {
            return Ok(_userService.GetUsers());
        }

        [Route("{id}")]
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            try
            {
                _userService.Delete(id);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPost]
        public ActionResult Post([FromBody] User user)
        {
            try
            {
                _userService.Create(user);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPut]
        public ActionResult Put([FromBody] User user)
        {
            try
            {
                _userService.Update(user);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

    }
}
