﻿using System;
using System.Threading.Tasks;

namespace ProjectConsole
{
    public class Menu
    {
        readonly string MainMenu = "Select menu item from 1 to 7 \n " +
            "1) Get user tasks per Project \n " +
            "2) Get list of tasks for certain user by Id where task name < 45 \n " +
            "3) Get List (id,name) of tasks which were finished in 2021 for user by Id \n " +
            "4) Get List of teams \n with performers older then 10 years grouped by registrations date and team \n " +
            "5) Get List of users with tasks grouped by name descending  \n " +
            "6) Get User and his last project, number of tasks for this project, total amount not finished tasks \n " +
            "7) Get List of projects with longest description task + shortest name task \n + amount of performers if description of task > 20 or amount of tasks is less then 3 \n " +
            "8) Exit";

        private static void Error(string error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Error! {error}");
            Console.ResetColor();
        }

        public async Task ShowMenu()
        {
            int Key = 0;
            Console.ResetColor();
            while (true)
            {
                Console.WriteLine(MainMenu);
                try
                {
                    Key = int.Parse(Console.ReadLine());
                    if (Key > 10 || Key < 1)
                    {
                        throw new ArgumentException("Wrong entry!");
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("No such item! Please select menu item from 1 to 10!");
                    Console.ResetColor();
                }

                switch (Key)
                {
                    case 1:
                        await ShowUserTasksPerProject();
                        break;
                    case 2:
                        await ShowListOfTasksForUser();
                        break;
                    case 3:
                        await ShowListOfTasksFinished();
                        break;
                    case 4:
                        await ShowTeams();
                        break;
                    case 5:
                        await ShowUsers();
                        break;
                    case 6:
                        await ShowUserLastProject();
                        break;
                    case 7:
                        await ShowProjects();
                        break;
                    case 8: return;

                }
            }
        }

        private async Task ShowUsers()
        {
            Console.Clear();
            var users = await RequestService.Task5();

            ShowBorder("Project with longest task by description + shortest task by name and count of performers");
            foreach (var user in users)
            {
                Console.WriteLine($"User id = {user.User.Id} name => ( {user.User.FirstName} ) last name =>( {user.User.LastName} )");
                Console.WriteLine("User tasks: ");
                foreach(var task in user.Tasks)
                {
                    Console.WriteLine($"Task id = {task.Id} name {task.Name}");
                }
                Console.WriteLine();
            }

        }

        private async Task ShowProjects()
        {
            Console.Clear();
            var projects = await RequestService.Task7();

            ShowBorder("Project with longest task by description + shortest task by name and count of performers");
            foreach(var project in projects)
            {
                Console.WriteLine($"Project id =>( {project.Project.Id} ) name =>( {project.Project.Name} ) \n" +
                    $"longest task by description is ( {project.LongestTaskDescription?.Description  ??  string.Empty} ) " +
                    $"\nShortest task by name is ( {project.ShortestTaskByName?.Name ?? string.Empty} )" +
                    $"\nPerformers number => [ {project?.UsersCount ?? 0} ]");
                Console.WriteLine();
            }
        }

        private async Task ShowUserLastProject()
        {
            int ID;
            while (true)
            {
                Console.Write("Please enter user Id: ");
                try
                {
                    ID = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            var user = await RequestService.Task6(ID);
            if( user == null)
            {
                ShowBorder($"There is no user with ID = [{ID}] with projects and tasks... ");
                return;
            }
            ShowBorder($"User id = {user.User.Id} name =>( {user.User.FirstName} ) last name => ( {user.User.LastName} )");
            Console.WriteLine($"Last project is => ( {user.LastProject.Name} ) created at {user.LastProject.CreatedAt}");
            Console.WriteLine($"Longest task is => ( {user.LongestTask.Name} ) created at {user.LongestTask.CreatedAt} and finished at {user.LongestTask.FinishedAt}");
            Console.WriteLine($"Number of unfinished tasks is => [ {user.UnFinishedTasks.Count} ]");

            Console.WriteLine();
        }

        private static void ShowBorder(string test)
        {
            Console.Clear();
            AnswerConsole();
            var temp = test;
            Console.WriteLine("╔" + new string('═', temp.Length) + "╗");
            Console.WriteLine("║" + temp + "║");
            Console.WriteLine("╚" + new string('═', temp.Length) + "╝");
            Console.ResetColor();
        }

        private async Task ShowTeams()
        {
            Console.Clear();
            var teams = await RequestService.Task4();

            ShowBorder("Teams with performers older when 10 years");

            foreach (var team in teams)
            {
                Console.WriteLine($"Team id: {team.Id} name=> ( {team.Name} ) create at {team.CreatedAt}");
                Console.WriteLine("Performers are:");
                foreach (var user in team.TeamPerformes)
                {
                    Console.WriteLine($"Perfromer ID: {user.Id} name {user.FirstName} last name {user.LastName}");
                }
                Console.WriteLine();
            }
        }

        static private void AnswerConsole()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }

        private async Task ShowListOfTasksFinished()
        {
            Console.Clear();
            int ID;
            while (true)
            {
                Console.Write("Please enter user Id: ");
                try
                {
                    ID = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            var tasks = await RequestService.Task3(ID);

            if (tasks.Count == 0)
            {
                ShowBorder($"User with id {ID} don't have finished tasks in 2021");
            }
            ShowBorder($"List of tasks which user with ID = {ID} finished in 2021");
            foreach (var task in tasks)
            {
                Console.WriteLine($"Task id:{task.Id} Task name: {task.Name} description: {task.Description}");
            }
            Console.WriteLine();
        }

        private async Task ShowListOfTasksForUser()
        {
            Console.Clear();
            int ID;
            while (true)
            {
                Console.Write("Please enter user Id: ");
                try
                {
                    ID = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            var tasks = await RequestService.Task2(ID);

            if (tasks.Count == 0)
            {
                ShowBorder($"User with id {ID} don't have tasks");
            }
            ShowBorder($"List of tasks for user {ID}");
            foreach (var task in tasks)
            {
                Console.WriteLine($"Task id:{task.Id} Task name: {task.Name} description: {task.Description}");
            }

            Console.WriteLine();
        }

        private async Task ShowUserTasksPerProject()
        {
            Console.Clear();
            int ID;
            while (true)
            {
                Console.Write("Please enter user Id: ");
                try
                {
                    ID = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            var tasks = await RequestService.Task1(ID);
     
            ShowBorder($"User with Id = {ID} tasks:");
            tasks = tasks.Replace("}", System.Environment.NewLine + " }");
            tasks = tasks.Replace(",", "," + System.Environment.NewLine);
            Console.WriteLine(tasks);
            Console.WriteLine();

        }
    }
}
