﻿using System;
using System.Threading.Tasks;

namespace ProjectConsole
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.ResetColor();
            Menu menu = new Menu();
            await menu.ShowMenu();
        }
    }
}
