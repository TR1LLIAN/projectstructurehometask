﻿using Newtonsoft.Json;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProjectConsole
{
    public class RequestService
    {


        static readonly HttpClient client = new HttpClient();
        const string Alias = "https://localhost:44320/api/Linq/";


        internal static async System.Threading.Tasks.Task<string> Task1(int id)
        {
            HttpResponseMessage response = await client.GetAsync(Alias + $"Task1/?id={id}");
            response.EnsureSuccessStatusCode();
            string projects = await response.Content.ReadAsStringAsync();
            return projects;
        }

        internal static async System.Threading.Tasks.Task<List<TaskModel>> Task2(int id)
        {
            HttpResponseMessage response = await client.GetAsync(Alias + $"Task2/?id={id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            var tasks = JsonConvert.DeserializeObject<List<TaskModel>>(temp);
            return tasks;
        }

        internal static async System.Threading.Tasks.Task<List<TaskModel>> Task3(int id)
        {
            HttpResponseMessage response = await client.GetAsync(Alias + $"Task3/?id={id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            var tasks = JsonConvert.DeserializeObject<List<TaskModel>>(temp);
            return tasks;
        }

        internal static async System.Threading.Tasks.Task<List<TeamModel>> Task4()
        {
            HttpResponseMessage response = await client.GetAsync(Alias + $"Task4/");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            var teams = JsonConvert.DeserializeObject<List<TeamModel>>(temp);
            return teams;
        }

        internal static async System.Threading.Tasks.Task<List<UserTasks>> Task5()
        {
            HttpResponseMessage response = await client.GetAsync(Alias + $"Task5/");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            var users = JsonConvert.DeserializeObject<List<UserTasks>>(temp);
            return users;

        }

        internal static async System.Threading.Tasks.Task<UserModel> Task6(int id)
        {
            HttpResponseMessage response = await client.GetAsync(Alias + $"Task6/?id={id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<UserModel>(temp);
            return user;
        }

        internal static async System.Threading.Tasks.Task<List<ProjectInfo>> Task7()
        {
            HttpResponseMessage response = await client.GetAsync(Alias + $"Task7/");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            var projects = JsonConvert.DeserializeObject<List<ProjectInfo>>(temp);
            return projects;
        }

        
    }


}
